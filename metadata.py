#!/usr/bin/env python3

from sys import argv
import requests
import json


talk_id = int(argv[1])

talk_req = requests.get('https://www.ted.com/talks/{}'.format(talk_id))
talk_html = map(str.strip, talk_req.text.split('\n'))
try:
    row_with_metadata = [i for i in filter(lambda x: '<script>q("talkPage.init",{"talks":[{"id":' in x, talk_html)][0] # rip out the right row
    indents = (row_with_metadata.index('{'), row_with_metadata[::-1].index('}')) # borders of json we need
    metadata = json.loads(row_with_metadata[indents[0]:][:-indents[1]])
    metadata['talks'][0]['targeting']['tag'] = metadata['talks'][0]['targeting']['tag'].split(',') # we want a proper list
    print(json.dumps(metadata['talks'][0]['targeting']))
    print(json.dumps(metadata['ratings']))
except IndexError: # page does not fit expected format
    pass
