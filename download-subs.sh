#!/usr/bin/sh
set -euo pipefail
IFS=$'\n\t'

for i in $(seq 1 $1);
	do python subs.py $i > data/subs/$i;
done;
