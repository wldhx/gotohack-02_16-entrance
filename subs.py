#!/usr/bin/env python3

import requests
import json
from sys import argv
from string import ascii_letters


sub_id = int(argv[1])

sub_req = requests.get('http://www.ted.com/talks/subtitles/id/{}/lang/eng/format/srt'.format(sub_id))
sub_text = sub_req.text.split()
words = filter(lambda word: word[0] in ascii_letters, sub_text) # TOFIX: will filter out e.g. '(word)'
print(json.dumps([i for i in words]))
