#!/usr/bin/sh
set -euo pipefail
IFS=$'\n\t'

for i in $(seq 1 $1);
	do python metadata.py $i > data/meta/$i;
done;
